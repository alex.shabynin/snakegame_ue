// Fill out your copyright notice in the Description page of Project Settings.


#include "Obsctacle.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AObsctacle::AObsctacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AObsctacle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AObsctacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AObsctacle::Interact(AActor* Interactor, bool bIsHead)
{
	IInteraction::Interact(Interactor, bIsHead);

	const auto Snake = Cast<ASnakeBase>(Interactor);

	if (IsValid(Snake))
	{
		if (!Snake->InObstacle)
		{
			Snake->InObstacle = true;
			Snake->Destroy();
			/*FVector MovementVector(FVector::ZeroVector);

			switch (Snake->LastMoveDirection)
			{
			case EMovementDirection::UP:
				MovementVector.X -= ObstacleDistance;
				break;
			case EMovementDirection::DOWN:
				MovementVector.X += ObstacleDistance;
				break;
			case EMovementDirection::LEFT:
				MovementVector.Y -= ObstacleDistance;
				break;
			case EMovementDirection::RIGHT:
				MovementVector.Y += ObstacleDistance;
				break;*/

		}
		Snake->InObstacle = false;

	}
	
}


