// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnBox.h"

// Sets default values
ASpawnBox::ASpawnBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SpawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Spawn Box"));
	RootComponent = SpawnBox;
}

void ASpawnBox::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	//Remove all timers in instance
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	//GetWorld()->GetTimerManager().ClearTimer(SpawnTimerHandle);
}

bool ASpawnBox::SpawnActor()
{
	bool SpawnedActor = false;
	if(ActorClassToBeSpawned)
	{
		//Calculate extends of collision box (root component of actor)
		FBoxSphereBounds BoxBounds = SpawnBox->CalcBounds(GetActorTransform());

		//Calculate random position within box
		FVector SpawnLocation = BoxBounds.Origin;
		SpawnLocation.X += -BoxBounds.BoxExtent.X + 2 * BoxBounds.BoxExtent.X * FMath::FRand();
		SpawnLocation.Y += -BoxBounds.BoxExtent.Y + 2 * BoxBounds.BoxExtent.Y * FMath::FRand();
		SpawnLocation.Z += -BoxBounds.BoxExtent.Z + 2 * BoxBounds.BoxExtent.Z * FMath::FRand();

		//Spawn actor
		SpawnedActor = GetWorld()->SpawnActor(ActorClassToBeSpawned, &SpawnLocation) !=nullptr;
	}
	return SpawnedActor;
}

void ASpawnBox::ScheduleActorSpawn()
{
	//Calcuulate time offset to spawn
	float DeltaToNextSpawn = AverageSpawnTime + (-RandomTimeOffset + 2 * RandomTimeOffset * FMath::FRand());
	//Schedule spawning
	GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &ASpawnBox::SpawnActorScheduled, DeltaToNextSpawn, false);
}

void ASpawnBox::SpawnActorScheduled()
{
//Spawn and reschedule if needed
	SpawnActor();
	if (ShouldSpawn)
	{
		ScheduleActorSpawn();
	}
}

// Called when the game starts or when spawned
void ASpawnBox::BeginPlay()
{
	Super::BeginPlay();

	if(ShouldSpawn)
	{
		ScheduleActorSpawn();
	}
	
}




