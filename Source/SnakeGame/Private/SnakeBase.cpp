// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interaction.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5f;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	
	FVector PrevLocation = FVector::ZeroVector;

	for (int i = 0; i < ElementsNum; ++i)
	{
		for (int j = SnakeElements.Num() - 1; j > 0; j--)
		{
			PrevElement = SnakeElements[j];
			PrevLocation = PrevElement->GetActorLocation();
		}

		FVector NewLocation(PrevLocation);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		const int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}

	/*for (int i = 0; i<ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0.f, 0.f);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		const int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if(ElemIndex==0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}*/
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	bool bSweep = true;
	//MovementSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
		
	}
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	for(int i = SnakeElements.Num()-1; i>0; i--)
	{
		const auto CurrentElement = SnakeElements[i];
		PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector, bSweep = true);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteraction* InteractableInterface = Cast<IInteraction>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SetMovementSpeed(float SpeedValue)
{
	MovementSpeed = SpeedValue;
}

