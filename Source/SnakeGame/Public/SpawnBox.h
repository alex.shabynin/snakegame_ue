// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Components/BoxComponent.h>
#include "SpawnBox.generated.h"


UCLASS()
class SNAKEGAME_API ASpawnBox : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnBox();

	//Function that will spawn actor of chosen class
	UFUNCTION(BlueprintCallable)
		bool SpawnActor();

		void ScheduleActorSpawn();

private:
	UFUNCTION()
		void SpawnActorScheduled();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Called when the actor stops playing
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	//Box, where we will spawn actors:
	UPROPERTY(EditDefaultsOnly)
		UBoxComponent* SpawnBox;

	//Here we will choose the actor, that we need to spawn:
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		TSubclassOf<AActor> ActorClassToBeSpawned;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool ShouldSpawn = true;

	//Spawning time parameters (time between each actor spawned):
	UPROPERTY(EditAnywhere)
		float AverageSpawnTime = 5.f;
	//Random offset between spawn time
	UPROPERTY(EditAnywhere)
		float RandomTimeOffset = 1.f;
	//Helper for timing
	FTimerHandle SpawnTimerHandle;
//public:	
//	// Called every frame
//	virtual void Tick(float DeltaTime) override;

};
